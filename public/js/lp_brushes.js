



function LPBrushCache(letsPaint) {
	var cache = [];

	function brushStatesEquiv(b1, b2) {
		// opacity and step do not affect brush image
		return (b1.size == b2.size && b1.hardness == b2.hardness && b1.color == b2.color);
	};

	function createCanvasForBrushState(brushState) {
		var c = hex2rgb(brushState.color);
		var r = c.r / 255.0;
		var g = c.g / 255.0;
		var b = c.b / 255.0;
		
		var hardnessFactor = 0.5 + Math.pow((brushState.hardness / 100.0), 4) * brushState.size / letsPaint.maxBrushSize * 50.0;

		var brushPrepareCanvas = document.createElement('canvas');
		brushPrepareCanvas.width = brushState.size;
		brushPrepareCanvas.height = brushState.size;
		var brushPrepareContext = createContextForCanvas(brushPrepareCanvas);
		var imageData = brushPrepareContext.createImageData(brushState.size, brushState.size);
		var data = imageData.data;
		var x;
		var y;
		var radius;
		var alpha;
		for (var i = 0; i < data.length; i += 4) {
			x = (i / 4) % brushState.size;
			y = ((i / 4) - x) / brushState.size;
			x /= brushState.size;
			y /= brushState.size;
			x -= 0.5;
			y -= 0.5;
			radius = 1.0 - 2.0 * Math.sqrt(x * x + y * y);
			alpha = clamp(radius);
			data[i] = r * 255;
			data[i + 1] = g * 255;
			data[i + 2] = b * 255;
			data[i + 3] = hardnessFactor * alpha * 255;
		}
		brushPrepareContext.putImageData(imageData, 0, 0);
		return brushPrepareCanvas;
	};

	this.imageCanvasForBrushState = function(brushState) {
		var cacheObject;
		for (var i = 0; i < cache.length; i++) {
			cacheObject = cache[i];
			if (brushStatesEquiv(cacheObject.state, brushState)) {
				if (i > 0) {
					// move object up in cache, swap i and i-1 in cache
					cache[i - 1] = cache.splice(i, 1, cache[i - 1])[0];
				}
				return cacheObject.canvas;
			}
		}
		// cache miss, create object
		cacheObject = {
			state: brushState,
			canvas: createCanvasForBrushState(brushState)
		};

		var indexToInsert = cache.length;
		if (indexToInsert >= letsPaint.brushCacheSize) {
			indexToInsert = letsPaint.brushCacheSize - Math.round(Math.random() * letsPaint.brushCacheSize / 2) - 1;
		}
		cache[indexToInsert] = cacheObject;

		return cacheObject.canvas;
	};
};

function LPBrushState(letsPaint, size, hardness, color, opacity, step) {
	
	size = Math.round(size);
	if (size > letsPaint.maxBrushSize) {
		size = letsPaint.maxBrushSize;
	}
	this.size = size;
	this.hardness = hardness;
	this.color = color;
	this.opacity = opacity;
	this.step = step;

	this.imageCanvas = function() {
		return letsPaint.brushCache.imageCanvasForBrushState(this);
	};
}
