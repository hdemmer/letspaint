

function LPBrushControl(letsPaint) {	
	var self = this;
	
	$('#brushControlDiv').html('<div id="brushCanvasDiv" \
	style="background: #fff;width: 256px;height: 256px;"></div> \
	<div class="label">Size:</div> \
	<div id="brushSize"></div> \
	<div class="label">Hardness:</div> \
	<div id="brushHardness"></div> \
	<div class="label">Opacity:</div> \
	<div id="brushOpacity"></div> \
	<div class="label">Step:</div> \
	<div id="brushStep"></div> \
	<div class="label">Color:</div> \
	<input id="colorPicker" type="text" name="color" value="000000"/>');
	
	var container = $('#brushCanvasDiv');
	
	var demoLayer = new LPLayer(letsPaint);
	demoLayer.canvas.width = letsPaint.maxBrushSize;
	demoLayer.canvas.height = letsPaint.maxBrushSize;
	container.append(demoLayer.canvas);

	var brushState;

	var brushSize = 128;
	var brushHardness = 50;
	var brushColor = '#000000';
	var brushOpacity = 1.0;
	var brushStep = 0.15;
	
	function demoBrush() {
		demoLayer.clearCanvas();
		$(demoLayer.canvas).css("opacity", brushState.opacity);
		demoLayer.paint(brushState, container.width() / 2.0, container.height() / 2.0, 0.5);
	};

	function reloadBrush() {
		brushState = new LPBrushState(letsPaint, brushSize, brushHardness, brushColor, brushOpacity, brushStep);
		demoBrush();
	};

	self.brushState = function() {
		if (!brushState)
		{
			reloadBrush();
		}
		return brushState;
	};
	
	self.updateBrush = function() {
		var size = $('#brushSize').val() / letsPaint.maxBrushSize;
		size = size * size;
		size *= letsPaint.maxBrushSize;
		brushSize = size;

		brushHardness = $('#brushHardness').val();
		brushOpacity = $('#brushOpacity').val() / 255.0;
		brushStep = $('#brushStep').val() / 100.0;
		brushColor = $('#colorPicker').val();
		
		reloadBrush();
	};

	// setup sliders
	$("#brushSize").replaceWith('<input id="brushSize" type="range" min="' + 4 + '" max="' + letsPaint.maxBrushSize + '" value="' + Math.sqrt(brushSize / letsPaint.maxBrushSize) * letsPaint.maxBrushSize + '"></input>');
	$('#brushSize')[0].onchange = self.updateBrush;
	$("#brushHardness").replaceWith('<input id="brushHardness" type="range" min="0" max="100" value="' + brushHardness + '"></input>');
	$('#brushHardness')[0].onchange = self.updateBrush;
	$("#brushOpacity").replaceWith('<input id="brushOpacity" type="range" min="0" max="255" value="' + brushOpacity * 255.0 + '"></input>');
	$('#brushOpacity')[0].onchange = self.updateBrush;
	$("#brushStep").replaceWith('<input id="brushStep" type="range" min="1" max="100" value="' + brushStep * 100.0 + '"></input>');
	$('#brushStep')[0].onchange = self.updateBrush;

	// color picker
	$('#colorPicker').val(brushColor);
	$("#colorPicker").minicolors('create', {
		inline: true,
		change: function(hex, opacity) {
			self.updateBrush();
		}
	});
}

function LPLayersControl(letsPaint) {
	var self = this;

	self.selectCurrentLayer = function(index) {
		letsPaint.painter.currentLayerIndex = index;
		self.updateLayersControl();
	}
		
	self.newLayer = function() {
		letsPaint.painter.currentLayerIndex = letsPaint.image.myLayersCount();
		self.updateLayersControl();
	}
	
	self.updateLayersControl = function() {
		function makeLayerElementForIndex(index) {
			var layerElement = $('<li class="layersListElement"></li>');
			var selectLayerButton = $('<button class="layersListElementButton button"></button>');
			selectLayerButton[0].onclick = function() {
				self.selectCurrentLayer(index);	
			};
			selectLayerButton.html('Layer ' + index);
			if (letsPaint.painter.currentLayerIndex == index) {
				selectLayerButton.addClass('button-active');
			}
			layerElement.append(selectLayerButton);
			return layerElement;
		}

		$('#layersList').remove();
		var layersList = $('<ul id="layersList"></ul>');
		$('#layersControlDiv').prepend(layersList);
		var lastLayerIndex = letsPaint.image.myLayersCount() - 1;
		if (letsPaint.painter.currentLayerIndex > lastLayerIndex)
		{
			// ephemeral new layer
			lastLayerIndex = letsPaint.painter.currentLayerIndex;
		}
		for (var i = lastLayerIndex; i >= 0; i--) {
			layersList.append(makeLayerElementForIndex(i));
		}
	}

	$('#layersControlDiv').append($('<button class="button" id="newLayerButton">New</button>'));
	$('#newLayerButton')[0].onclick = function() {
		self.newLayer();
	};
}


function LPSpinner() {
	$('#layersControlDiv').append($('<div id="spinner" style="height:44px;"></div>'));

	var opts = {
		lines: 13,
		// The number of lines to draw
		length: 7,
		// The length of each line
		width: 4,
		// The line thickness
		radius: 10,
		// The radius of the inner circle
		corners: 1,
		// Corner roundness (0..1)
		rotate: 0,
		// The rotation offset
		color: '#eee',
		// #rgb or #rrggbb
		speed: 1,
		// Rounds per second
		trail: 60,
		// Afterglow percentage
		shadow: true,
		// Whether to render a shadow
		hwaccel: false,
		// Whether to use hardware acceleration
		className: 'spinner',
		// The CSS class to assign to the spinner
		zIndex: 2e9,
		// The z-index (defaults to 2000000000)
		top: 'auto',
		// Top position relative to parent in px
		left: 'auto' // Left position relative to parent in px
	};
	var spinner = new Spinner(opts);
	this.start = function() {
		var target = document.getElementById('spinner');
		spinner.spin(target);
	};
	this.stop = function() {
		spinner.stop();

	};
};


function LPNavigationControl(letsPaint) {
	var self = this;
	$('#navigationDiv').html('<div class="label">Zoom:</div><div id="zoomSliderDiv"></div>');

	var container = $('#navigationContainer');

	self.zoomChanged = function() {
		var value = $('#zoomSlider').val() / 100.0;
		letsPaint.painter.setZoom(value);
	};

	$("#zoomSliderDiv").append($('<input id="zoomSlider" type="range" min="10" max="100" value="100">'));
	$('#zoomSlider')[0].onchange = function() {
		self.zoomChanged();
	};
};

function LPUtensilsControl(letsPaint) {
	var self = this;
	var utensils = [letsPaint.PAINT,letsPaint.PAN,letsPaint.PICK,letsPaint.ERASE];
		
	self.update = function() {
		$('#utensilsDiv').html('<div class="toolbar">Tool</div>');
		
		var utensilsDivWidth = $('#utensilsDiv').innerWidth() - 2;
		$.each(utensils, function(index,utensil){
			var button = $('<button class="button">'+utensil+'</button>');
			button.width(utensilsDivWidth);
			button.height(utensilsDivWidth);
			
			var state = letsPaint.painter.stateForUtensil(utensil);
			if (state == 'current')
			{
				button.addClass('button-active');
			}
			if (state == 'quick')
			{
				button.addClass('button-hover');
			}
			
			button[0].onclick = function () {
				letsPaint.painter.currentUtensil = utensil;
				self.update();
			};
			$('#utensilsDiv').append(button);			
		});
		
		$('#utensilsDiv').append($('<button class="button" id="downloadButton" style="position:absolute;left:0px;top:100%;margin-top:'+(-utensilsDivWidth)+'px;">Download</button>'));
		var button = $('#downloadButton');
		button.width(utensilsDivWidth);
		button.height(utensilsDivWidth);
		button[0].onclick = function() {
			letsPaint.image.downloadImage();
		};
	}
};	
