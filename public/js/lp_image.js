

function LPLayer(letsPaint) {
	var self = this;

	var canvas = document.createElement('canvas');
	canvas.width = letsPaint.imageWidth;
	canvas.height = letsPaint.imageHeight;
	
	$(canvas).css('position', 'absolute');
	self.canvas = canvas;

	self.setZIndex = function(zIndex) {
		$(canvas).css("z-index", zIndex);
	};

	var context = createContextForCanvas(canvas);

	var opacity;
	self.opacity = function(value) {
		if (value != undefined) {
			opacity = value;
			$(self.canvas).css("opacity", letsPaint.brushControl.brushState().opacity);

		}
		return opacity;
	};

	var mode;
	self.mode = function(value) {
		if (value != undefined) {
			mode = value;
		}
		return mode;
	};

	self.paint = function(brushState, x, y, p) { // p is pressure
		var size = brushState.size * p;
		context.drawImage(brushState.imageCanvas(), x - size / 2.0, y - size / 2.0, size, size);
	};

	self.clearCanvas = function() {
		canvas.width = canvas.width;
	};

	var brushStrokes = new Array();
	var currentRev = -1;

	self.lastPaintedRev = -1;

	self.paintStroke = function(brushStroke) {
		var compLayer = new LPLayer(letsPaint);
		compLayer.opacity(brushStroke.state.opacity);
		compLayer.mode(brushStroke.action);
		$.each(brushStroke.points, function(index, point) {
			compLayer.paint(brushStroke.state, point.x, point.y, point.p);
		});

		self.paintLayer(compLayer);
	};

	self.paintLayer = function(layer) {
		if (layer.mode() == letsPaint.ERASE) {
			context.globalCompositeOperation = 'destination-out';
		}
		context.globalAlpha = layer.opacity() || 1.0;
		context.drawImage(layer.canvas, 0, 0);
		context.globalAlpha = 1.0;
		context.globalCompositeOperation = 'source-over';
	};

	self.copyLayer = function(layer) {
		context.globalAlpha = 1.0;
		context.globalCompositeOperation = 'copy';
		context.drawImage(layer.canvas, 0, 0);
		context.globalCompositeOperation = 'source-over';
	};

	self.drawMyPixelIntoContext = function(context, x, y) {
		context.drawImage(canvas, x, y, 1, 1, 0, 0, 1, 1);
	};
};

function LPImage(letsPaint) {
	var self = this;

	self.width = letsPaint.imageWidth;
	self.height = letsPaint.imageHeight;
	self.maxLayers = 100;

	var layers = []; // userId:index
	self.layer = function(userId, layerIndex) {
		if (!layers[userId]) {
			layers[userId] = [];
		}
		if (!layers[userId][layerIndex]) {
			// create layer
			var layer = new LPLayer(letsPaint);
			layer.setZIndex(self.zIndexForLayer(userId, layerIndex));
			letsPaint.painter.insertLayer(layer);
			layers[userId][layerIndex] = layer;
			
			letsPaint.layersControl.updateLayersControl();
		}
		return layers[userId][layerIndex];
	};

	self.zIndexForLayer = function(userId, layerIndex) {
		return (userId * self.maxLayers + layerIndex) * 10;
	};
	
	self.myLayersCount = function() {
		return (layers[letsPaint.network.userId()] || []).length;
	}

	self.myLayer = function(layerIndex) {
		return self.layer(letsPaint.network.userId(), layerIndex);
	};

	self.zIndexForMyLayer = function(layerIndex) {
		return self.zIndexForLayer(letsPaint.network.userId(), layerIndex);
	};
	
	function forAllLayers(call) {
		for (var i = 0; i < layers.length; i++) {
			if (layers[i]) {
				for (var j = 0; j < layers[i].length; j++) {
					var layer = layers[i][j];
					if (layer) {
						call(layer);
					}
				}
			}

		}
	}

	self.pickColor = function(x, y) {
		var canvas = document.createElement('canvas');
		canvas.width = 1;
		canvas.height = 1;
		var context = createContextForCanvas(canvas);

		context.fillStyle = '#fff';
		context.fillRect(0, 0, 1, 1);

		forAllLayers(function(layer) {
			layer.drawMyPixelIntoContext(context, x, y);
		});
		var data = context.getImageData(0, 0, 1, 1).data;
		return "#" + ((1 << 24) + (data[0] << 16) + (data[1] << 8) + data[2]).toString(16).slice(1);
	};

	self.downloadImage = function() {
		var compositeLayer = new LPLayer(letsPaint);

		forAllLayers(function(layer) {
			compositeLayer.paintLayer(layer);
		});

		window.open(compositeLayer.canvas.toDataURL());
	};
};

