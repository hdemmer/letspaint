

function LPNetworkConnection(letsPaint) {
	var self = this;
	
	var brushStateDirty = true;
	var brushStateCache;
	var userId = -1;

	self.userId = function() {
		return userId;
	};

	function getURLParameter(name) {
		return decodeURI((RegExp(name + '=' + '(.+?)(&|$)').exec(location.search) || [, null])[1]);
	};

	self.userName = getURLParameter('userName');
	self.roomName = getURLParameter('roomName');

	function trimNilpotentLayerStrokes(strokes) {
		// first, remove undo/redo
		for (var i = 0; i < strokes.length - 2; i++) {
			if (i < 0) {
				i = 0;
			}
			if (strokes[i].action == letsPaint.UNDO && strokes[i + 1].action == letsPaint.REDO) {
				strokes.splice(i, 2);
				i -= 2;
			}
		}

		// now remove undone strokes
		for (var i2 = 0; i2 < strokes.length - 2; i2++) {
			if (i2 < 0) {
				i2 = 0;
			}
			if (!strokes[i2].action && strokes[i2 + 1].action == letsPaint.UNDO) {
				strokes.splice(i2, 2);
				i2 -= 2;
			}
		}

		return strokes;
	};

	if (typeof(io) != 'undefined') // is socket.io available?
	{
		var socket = io.connect(letspaintPaintHost);
		socket.on('welcome', function(data) {
			socket.emit('connectInfo', {
				roomName: self.roomName,
				userName: self.userName
			});
		});
		socket.on('userId', function(data) {
			userId = parseInt(data, 10);
			letsPaint.image.networkReceivedUserId();
			letsPaint.chat.displayInfoMessage("Joined channel. UserID is " + userId);
		});

		function pushAndPaintRemoteStroke(stroke) {
			var layer = letsPaint.image.layer(stroke.userId, stroke.layerIndex);

			if (stroke.action == letsPaint.UNDO) {
				layer.undo();
			} else if (stroke.action == letsPaint.REDO) {
				layer.redo();
			} else {
				stroke.state = new LPBrushState(letsPaint, stroke.state.size, stroke.state.hardness, stroke.state.color, stroke.state.opacity, stroke.state.step);
				layer.pushStroke(stroke);
			}
		};
		socket.on('userJoined', function(data) {
			var id = parseInt(data, 10);
			letsPaint.chat.displayInfoMessage("User joined: " + id);
		});
		socket.on('userLeft', function(data) {
			var id = parseInt(data, 10);
			letsPaint.chat.displayInfoMessage("User left: " + id);
		});
		socket.on('layer', function(data) {
			var layerStrokes = trimNilpotentLayerStrokes(data);
			for (var rev = 0; rev < layerStrokes.length; rev++) {
				var stroke = layerStrokes[rev];
				pushAndPaintRemoteStroke(stroke);
			}
		});
		socket.on('brushStroke', function(data) {
			pushAndPaintRemoteStroke(data);
		});
		self.postChatMessage = function(text) {
			socket.emit('chat', {
				userId: userId,
				text: text
			});
		};
		socket.on('chat', function(data) {
			letsPaint.chat.displayChatMessage(data.userId + ': ' + data.text);
		});
		socket.on('error', function(data) {
			letsPaint.displayError(data);
		});
	}

	this.writeBrushStroke = function(brushStroke) {
		if (socket) {
			brushStroke.userId = userId;
			brushStroke.layerIndex = letsPaint.painter.currentLayerIndex;
			socket.emit('brushStroke', brushStroke);
		}
	};
};