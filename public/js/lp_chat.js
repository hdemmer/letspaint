function LPChat(letsPaint) {
	var self = this;
	
	var container = $('#chatDiv');
	container.append($('<textarea class="textinput" readonly id="chatText"></textarea> \
	<input type="text" class="textinput" id="chatInput"> \
	<button class="button" id="chatPostButton" style="width:5em;">Post</button>'));
	$('#chatPostButton')[0].onclick=function(){
		self.postChatMessage();
	};
	self.resize = function () {
		var w = container.width();
		var h = container.height();
		
		$('#chatText').width(w-15);
		$('#chatText').height(h-40);
		$('#chatInput').width(w-80);
	};

	self.postChatMessage = function() {
		var text = $('#chatInput').val();
		if (text != '') {
			letsPaint.network.postChatMessage(text);
			self.displayChatMessage('me: ' + text);
			$('#chatInput').val('');
		}
	};
	self.displayChatMessage = function(text) {
		var textArea = $('#chatText');
		textArea.val(textArea.val() + text + '\n');
		textArea.scrollTop(textArea[0].scrollHeight);
	};
	
	self.displayInfoMessage=self.displayChatMessage;

	$("#chatInput").keypress(function(e) {
		if (e.keyCode == 13) {
			self.postChatMessage();
		}
	});
}