
var LPLobby = function() {
	var activityStreamDiv = $('#activityStreamDiv');
	activityStreamDiv.append($('<textarea class="textinput" readonly id="activityText" style="width:320px;height:480px"></textarea>'));
	if (typeof(io) != 'undefined') // is socket.io available?
	{
		var socket = io.connect(letspaintLobbyHost);
		socket.on('welcome', function(data) {
			socket.emit('subscribeActivities', {});
			socket.emit('getRoomList', {});
		});
		
		socket.on('roomList', function(data) {
			var val=$('#roomSelect').val();
			
			$('#roomSelect').html('<option disabled="disabled">Choose a room</option>');
			for (var i in data) {
				var room = data[i];
				var name = room['name'];
				var userCount = room['userCount'] || 0;
				$('#roomSelect').append('<option value=' + encodeURI(name) + '>' + name + ' (' + userCount + ')</option>');
			}
			if (val)
			{
				$('#roomSelect').val(val);
			}
			roomSelectionChanged();
		});

		socket.on('roomActivity', function(data) {
			$('#activityText').val($('#activityText').val() + data + '\n');
			socket.emit('getRoomList', {});
		});
	}
};


// load socket.io
var script = document.createElement('script');
script.type = 'text/javascript';
script.src = letspaintLobbyHost + '/socket.io/socket.io.js';
var head = document.getElementsByTagName('head')[0];
head.appendChild(script);

// when socket.io is loaded, load the painter
script.onload = function() {
	$(function() {
		var lobby = new LPLobby();
	});
};
