var LetsPaint = function() {

		function LPPainter(letsPaint) {
			var self = this;
			var lpContainer = $('#letsPaintContainer');
			var canvasDiv = $('<div id="canvasDiv" style="position:absolute;"></div>');
			canvasDiv.appendTo(lpContainer);
			canvasDiv.css('overflow', 'auto');

			var container = $('<div style="background:#fff"></div>');
			container.appendTo(canvasDiv);
			container.width(letsPaint.imageWidth);
			container.height(letsPaint.imageHeight);

			var currentScale = 1;

			self.setZoom = function(value) {

				var scrollTop = canvasDiv.scrollTop();
				var scrollLeft = canvasDiv.scrollLeft();

				var viewPortWidth = canvasDiv.width();
				var viewPortHeight = canvasDiv.height();
				var imageWidth = container.width();
				var imageHeight = container.height();

				var viewCenterInCanvasX = scrollLeft + viewPortWidth * 0.5 - imageWidth * 0.5;
				viewCenterInCanvasX /= currentScale * imageWidth;
				var viewCenterInCanvasY = scrollTop + viewPortHeight * 0.5 - imageHeight * 0.5;
				viewCenterInCanvasY /= currentScale * imageHeight;

				scrollLeft += viewCenterInCanvasX * imageWidth * (value - currentScale);
				scrollTop += viewCenterInCanvasY * imageHeight * (value - currentScale);

				canvasDiv.scrollTop(scrollTop);
				canvasDiv.scrollLeft(scrollLeft);

				container.css('transform', 'scale(' + value + ',' + value + ')');
				currentScale = value;
			};

			self.insertLayer = function(layer) {
				container.append(layer.canvas);
			};

			self.currentLayerIndex = 0;
			self.currentLayerForPainting = function() {
				return letsPaint.image.myLayer(self.currentLayerIndex);
			};

			var currentAction = undefined;
			self.currentUtensil = LetsPaint.PAINT;

			var quickTool = undefined;

			function updateCursor() {
				var cursorsForTools = {
					paint: 'crosshair',
					pan: 'move',
					pick: 'pointer',
					erase: 'auto'
				};
				if (currentAction) {
					canvasDiv[0].style.cursor = cursorsForTools[currentAction];
				} else if (quickTool) {
					canvasDiv[0].style.cursor = cursorsForTools[quickTool];
				} else {
					canvasDiv[0].style.cursor = 'auto';
				}
			};
			self.quickTool = function(toolName) {
				quickTool = toolName;
				updateCursor();
				letsPaint.utensilsControl.update();
			};
			
			self.stateForUtensil = function(toolName) {
				if (toolName == self.currentUtensil)
				{
					return 'current';
				}
				if (toolName == quickTool)
				{
					return 'quick';
				}
				return undefined;
			};

			// setup hot keys
			$(document).keydown(function(e) {
				if (document.activeElement != document.body) {
					return;
				}
				if (e.which == 16) {
					self.quickTool('erase');
				}
				if (e.which == 32) {
					if (e.stopPropagation) {
						e.stopPropagation();
					}
					e.preventDefault();
					self.quickTool('pan');
				}
				if (e.which == 18) {
					self.quickTool('pick');
				}
			});

			$(document).keyup(function(e) {
				if (document.activeElement != document.body) {
					return;
				}
				if (e.which == 16) {
					self.quickTool(undefined);
				}
				if (e.which == 32) {
					self.quickTool(undefined);
				}
				if (e.which == 18) {
					self.quickTool(undefined);
				}
			});

			$(document).mouseleave(function(e) {
				self.quickTool(undefined);
			});

			// setup painter
			var previewLayer = new LPLayer(letsPaint);
			self.insertLayer(previewLayer);
			var previewPrepareLayer = new LPLayer(letsPaint); // off-screen layer to paint preview in
			var currentStroke = {};
			var lastMouseX = -1;
			var lastMouseY = -1;
			var lastPressure = 1;
			var mouseX = -1;
			var mouseY = -1;
			var pressure = 1;
			var distance = undefined;
			var usePen = false;

			function remapPressure(p) {
				var p2 = (Math.sqrt(p + 1.0) - 1.0) * 0.9 + 0.1; // TODO: min brush size
				return p2;
			}

			function updateMouse(e) {
				var parentOffset = container.offset();
				lastMouseX = mouseX;
				lastMouseY = mouseY;

				mouseX = (e.pageX - parentOffset.left - container.scrollLeft()) / currentScale;
				mouseY = (e.pageY - parentOffset.top - container.scrollTop()) / currentScale;

				var dx = mouseX - lastMouseX;
				var dy = mouseY - lastMouseY;
				distance += Math.sqrt(dx * dx + dy * dy);

				// get pressure etc from wacom
				lastPressure = pressure;
				if (usePen) {
					var wacomPlugin = document.getElementById('wtPlugin');
					if (wacomPlugin) {
						var penAPI = wacomPlugin.penAPI;
						pressure = remapPressure(penAPI.pressure);
					}
				} else {
					pressure = 1.0;
				}
			}

			function updateMouseWithTouch(e) {
				var parentOffset = container.offset();
				lastMouseX = mouseX;
				lastMouseY = mouseY;

				mouseX = (e.targetTouches[0].pageX - parentOffset.left - container.scrollLeft()) / currentScale;
				mouseY = (e.targetTouches[0].pageY - parentOffset.top - container.scrollTop()) / currentScale;

				var dx = mouseX - lastMouseX;
				var dy = mouseY - lastMouseY;
				distance += Math.sqrt(dx * dx + dy * dy);

				lastPressure = 0.5;
				pressure = 0.5;
			}

			function resetDistance() {
				lastMouseX = mouseX;
				lastMouseY = mouseY;
				distance = undefined;
				lastPressure = 0.0;
			}

			function startPainting() {
				currentStroke = {
					state: letsPaint.brushControl.brushState(),
					points: [],
					action: currentAction
				};
				previewLayer.setZIndex(letsPaint.image.zIndexForMyLayer(self.currentLayerIndex) + 1);
				if (currentStroke.action == letsPaint.ERASE) {
					previewLayer.opacity(self.currentLayerForPainting().opacity());
					previewPrepareLayer.opacity(letsPaint.brushControl.brushState().opacity);
					$(self.currentLayerForPainting().canvas).css('visibility', 'hidden');
				} else {
					previewLayer.opacity(1.0);
					previewPrepareLayer.opacity(letsPaint.brushControl.brushState().opacity);
				}
				previewPrepareLayer.mode(currentStroke.action);
				previewLayer.mode(letsPaint.PAINT);

				paintNow(); // ensure a stroke point right here
			}

			function stopPainting() {
				self.currentLayerForPainting().paintLayer(previewPrepareLayer);
				letsPaint.network.writeBrushStroke(currentStroke);
				if (currentStroke.action == letsPaint.ERASE) {
					$(self.currentLayerForPainting().canvas).css('visibility', 'visible');
				}
				currentStroke = {};
				currentAction = undefined;
				previewLayer.clearCanvas();
				previewPrepareLayer.clearCanvas();
			};

			function paintNow() {
				var brushState = letsPaint.brushControl.brushState();
				var step = brushState.size * brushState.step;

				var minP = pressure;
				if (lastPressure < minP) {
					minP = lastPressure;
				}
				if (minP < 0.1) {
					minP = 0.1;
				}
				step *= minP; // reduce step for low pressure
				var erasing = (currentStroke.action == letsPaint.ERASE);

				if (distance == undefined) {	// ensure that the first click triggers a paint
					distance = step;
				}
				var numSteps = Math.floor((distance) / step);

				for (var it = 0; it < numSteps; it++) {
					var lambda = (it / numSteps);
					var x = lastMouseX * lambda + mouseX * (1 - lambda);
					var y = lastMouseY * lambda + mouseY * (1 - lambda);
					var p = lastPressure * lambda + pressure * (1 - lambda);

					// reduce float precision
					p = Math.round(p * 1000) * 0.001;
					x = Math.round(x * 1000) * 0.001;
					y = Math.round(y * 1000) * 0.001;

					previewPrepareLayer.paint(brushState, x, y, p);
					currentStroke.points.push({
						x: x,
						y: y,
						p: p
					});
					distance -= step;
				}

				if (erasing) {
					previewLayer.clearCanvas();
					previewLayer.copyLayer(self.currentLayerForPainting());
					previewLayer.paintLayer(previewPrepareLayer);
				} else {
					previewLayer.copyLayer(previewPrepareLayer);
				}
			}

			function panNow() {
				var scrollTop = canvasDiv.scrollTop();
				var scrollLeft = canvasDiv.scrollLeft();

				var dX = mouseX - lastMouseX;
				var dY = mouseY - lastMouseY;
				scrollTop -= dY * currentScale;
				scrollLeft -= dX * currentScale;
				mouseX -= dX;
				mouseY -= dY;

				canvasDiv.scrollTop(scrollTop);
				canvasDiv.scrollLeft(scrollLeft);
			}

			function pickNow() {
				var color = letsPaint.image.pickColor(mouseX, mouseY);
				var pick = $('#colorPicker');
				pick.minicolors('value',color);
				letsPaint.brushControl.updateBrush();
			}
			// setup event listeners
			container.mousedown(function(event) {
				event.preventDefault();
				if (currentAction) {	// we're already doing something?
					return;
				}

				if (quickTool) {
					currentAction = quickTool;
				} else {
					currentAction = self.currentUtensil;
				}
				
				var wacomPlugin = document.getElementById('wtPlugin');
				if (wacomPlugin) {
					if (wacomPlugin.penAPI.pressure > 0.0) {
						usePen = true;
					} else {
						usePen = false;
					}
					if (wacomPlugin.penAPI.isEraser) {
						currentAction = letsPaint.ERASE;
					}
				}
				updateMouse(event);
				resetDistance();

				if (currentAction == letsPaint.PICK) {
					pickNow();
				}
				
				if (currentAction == letsPaint.PAINT || currentAction == letsPaint.ERASE)
				{
					startPainting();
				}

				updateCursor();
			});
			container.mousemove(function(event) {
				event.preventDefault();
				updateMouse(event);
				if (currentAction == letsPaint.PAINT || currentAction == letsPaint.ERASE) {
					paintNow();
				} else if (currentAction == letsPaint.PAN) {
					panNow();
				} else if (currentAction == letsPaint.PICK) {
					pickNow();
				}
			});
			var stopAction = function(event) {
					event.preventDefault();
					if (currentAction == letsPaint.PAINT || currentAction == letsPaint.ERASE) {
						stopPainting();
					}
					currentAction = undefined;
					updateCursor();
				};
			container.mouseup(stopAction);
			container.mouseleave(stopAction);

			// setup touch listeners

			function touchstart(e) {
				e.preventDefault();
				updateMouseWithTouch(e);
				resetDistance();
				startPainting();
			};

			function touchmove(e) {
				e.preventDefault();
				updateMouseWithTouch(e);
				paintNow();
			};

			function touchend(e) {
				e.preventDefault();
				stopPainting();
			};

			function touchcancel(e) {
				e.preventDefault();
				stopPainting();
			};

			container[0].addEventListener("touchstart", touchstart, false);
			container[0].addEventListener("touchmove", touchmove, false);
			container[0].addEventListener("touchend", touchend, false);
			container[0].addEventListener("touchcancel", touchcancel, false);
		}

		// setup
		var letsPaint = document.createElement('custom');
		letsPaint.setAttribute('id', 'letsPaint');
		document.body.appendChild(letsPaint);

		letsPaint.imageWidth = 2000;
		letsPaint.imageHeight = 1600;
		letsPaint.brushCacheSize = 64;
		letsPaint.maxBrushSize = 256;

		// create divs
		
		$('#letsPaintDiv').html('<div id="letsPaintContainer"> \
		</div> \
		<div id="utensilsDiv" style="position:absolute;overflow:hidden;"></div> \
		<div id="chatSlideable" style="position:absolute;overflow:hidden;"> \
		<div id="chatDiv" class="utensils" style="overflow:hidden;"></div> \
		<div id="chatSlideHandle" class="toolbar">\
			<button id="chatSlideButton" class="button" style="float:left;">Chat</button></div> \
		</div> \
		<div id="toolsContainer" style="position:absolute;overflow-y:auto;overflow-x:hidden;"> \
			<div class="toolbar">Navigation</div> \
			<div id="navigationDiv"> \
			</div> \
			<div class="toolbar">Brush</div> \
			<div id="brushControlDiv"> \
			</div> \
			<div class="toolbar">Layers</div> \
			<div id="layersControlDiv"></div> \
			</div>\
		</div>');

		// consts
		letsPaint.PAINT = 'paint';
		letsPaint.PAN = 'pan';
		letsPaint.PICK = 'pick';
		letsPaint.ERASE = 'erase';

		// create cpmponents
		letsPaint.image = new LPImage(letsPaint);
		letsPaint.painter = new LPPainter(letsPaint);
		letsPaint.brushCache = new LPBrushCache(letsPaint);
		letsPaint.network = new LPNetworkConnection(letsPaint);
		letsPaint.spinner = new LPSpinner(letsPaint);
		letsPaint.chat = new LPChat(letsPaint);

		letsPaint.brushControl = new LPBrushControl(letsPaint);
		letsPaint.layersControl = new LPLayersControl(letsPaint);
		letsPaint.navigationControl = new LPNavigationControl(letsPaint);
		letsPaint.utensilsControl = new LPUtensilsControl(letsPaint);
				
		letsPaint.displayError = function(message) {
			$('#letsPaintDiv').html('<div class="error"><h1>Error</h1><div>'+message+'</div>\
			<button class="button" onclick="history.back()">Go back</button>\
			<button class="button" onclick="location.reload()">Retry</button>\
			</div>');
		};
		// UI

		var chatSlidOut = false;

		function resizeUI() {
			var w = window.innerWidth;
			var h = window.innerHeight;
			
			var toolsContainerWidth = 256;
			var scrollBarWidth = 16;
			var utensilsWidth = 44;
			
			var chatSlideY = (chatSlidOut?h*0.25:0);
			var chatSlideHandleHeight = $('#chatSlideHandle').outerHeight();

			$("#canvasDiv").width(w - toolsContainerWidth - utensilsWidth - scrollBarWidth);
			$("#canvasDiv").height(h - chatSlideY - chatSlideHandleHeight);
			$("#canvasDiv").css('left', utensilsWidth);
			$("#canvasDiv").css('top',chatSlideY + chatSlideHandleHeight);

			$("#utensilsDiv").height(h - chatSlideY - chatSlideHandleHeight);
			$("#utensilsDiv").css('top',chatSlideY + chatSlideHandleHeight);
			$("#utensilsDiv").css('left',0);
			$("#utensilsDiv").width(utensilsWidth);

			$("#toolsContainer").css('left', w - toolsContainerWidth - scrollBarWidth);
			$("#toolsContainer").css('top', 0);
			$("#toolsContainer").width(toolsContainerWidth + scrollBarWidth);
			$("#toolsContainer").outerHeight(h);

			$('#chatSlideHandle').width(w - toolsContainerWidth - scrollBarWidth);
			$('#chatSlideable').width(w - toolsContainerWidth);
			$('#chatDiv').outerHeight(chatSlideY);
			$('#chatSlideable').outerHeight(chatSlideHandleHeight+chatSlideY);
			$("#chatSlideable").css('top', -$('#chatSlideable').outerHeight()+chatSlideHandleHeight+chatSlideY);
			letsPaint.chat.resize();
			
			// TODO: this is really sloppy: we resize all sliders to a magic number width
			$("[type='range']").each(function(i,element) {
				$(element).outerWidth(254);
			});
			
		};
		$('#chatSlideButton')[0].onclick = function() {
			chatSlidOut = !chatSlidOut;
			resizeUI();
		};

		// final init
		
		resizeUI();
		letsPaint.brushControl.updateBrush();
		letsPaint.painter.currentUtensil = 'paint';
		letsPaint.painter.setZoom(1.0);
		letsPaint.layersControl.updateLayersControl();
		letsPaint.utensilsControl.update();
		
		window.setInterval(resizeUI, 1000);

	}

// when socket.io is loaded, load the painter
	$(function() {
		letsPaint = new LetsPaint();
	});
