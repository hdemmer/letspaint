Let's Paint
===========

Tired of OpenCanvas running on Windows only? Frustrated with the limited brushes of flockdraw? *Let's Paint* is a browser app for painting with friends (or strangers?) online.

Tech
----
 * `canvas` element
 * node.js server
 * setTimeout-based worker for responsiveness

Features
--------

 * Photoshop-inspired brush model
 * Wacom-support out of the box
 * Compatible with touch devices including iPad

Try it now!
-----------

A live demo is hosted [here](http://www.desirableartifacts.com/letspaint).

Set up your own
---------------

 1. Start the `server` with `node server.js`, ensure that ports `8080` and `8081` are accessible.
 2. Point the URLs in `public/js/config.js` to your server
 3. Host everything in `public` somewhere, does not have to be the same server or even bas URL.
 4. Tweak the settings in `server.js` to your liking.

License
-------

Let's Paint is open source under the MIT license.
