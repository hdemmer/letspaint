var lobbyPort = 8080;
var paintPort = 8081;

var maxLobbyListenersCount = 64;
var maxRooms = 16;
var maxUsersPerRoom = 8;

function Room(name) {
	self = this;
	self.name = name;

	self.layerStrokes = [];
	var users = {};

	var user_id = 0;
	var User = function(name) {
			this.name = name;
			this.id = user_id;
			user_id += 1;
		};

	self.userForName = function(userName) {
		if (!users[userName]) {
			users[userName] = new User(userName);
		}
		return users[userName];
	};

	self.emitFrom = function(originatingUser, messageName, data) {
		for (var userName in users) {
			var user = users[userName];
			if (user != originatingUser) {
				if (user.socket) // still connected?
				{
					user.socket.emit(messageName, data);
				}
			}
		}
	};

	self.numberOfActiveUsers = function() {
		var num = 0;
		for (var userName in users) {
			var user = users[userName];
			if (user.socket) // still connected?
			{
				num++;
			}
		}
		return num;
	};

	// returns the same structure as layerStrokes, but leaves are not strokes, but their count
	self.state = function() {
		return self.layerStrokes.map(function(userLayerStrokes) {
			return userLayerStrokes.map(function(strokes) {
				return strokes.length;
			});
		});
	};
};

var ActivityStream = function() {
		var self = this;

		var listeners = [];
		var activityBuffer = ['Server started.'];

		self.maxBufferLength = 256;

		self.postActivity = function(activity) {
			console.log('Activity: ' + activity);
			activityBuffer.push(activity);
			if (activityBuffer.length > self.maxBufferLength) {
				activityBuffer.shift();
			}
			for (var i in listeners) {
				listeners[i].emit('roomActivity', activity);
			}
		};
		self.addListener = function(socket) {
			console.log('Adding activityStream listener: ' + socket.handshake.address.address);
			for (var i in activityBuffer) {
				socket.emit('roomActivity', activityBuffer[i]);
			}
			listeners.push(socket);
		};
		self.removeListener = function(socket) {
			console.log('Lost activityStream listener: ' + socket.handshake.address.address);
			var index = listeners.indexOf(socket);
			if (index) {
				listeners.splice(index, 1);
			}
		};
		self.numberOfListeners = function(socket) {
			return listeners.length;
		};
	};

var activityStream = new ActivityStream();
var rooms = {};

var lobbyIO = require('socket.io').listen(lobbyPort);

lobbyIO.enable('browser client minification'); // send minified client
lobbyIO.enable('browser client etag'); // apply etag caching logic based on version number
lobbyIO.enable('browser client gzip'); // gzip the file
lobbyIO.set('log level', 1); // reduce logging
lobbyIO.sockets.on('connection', function(socket) {

	function sendRoomList() {
		var roomNames = [];
		for (room in rooms) {
			roomNames.push({
				name: room,
				userCount: rooms[room].numberOfActiveUsers()
			});
		}
		console.log("Serving room list, " + roomNames.length + ' entries.');
		socket.emit('roomList', roomNames);
	};

	socket.emit('welcome', '');

	// don't use live feed if server is busy
	if (activityStream.numberOfListeners >= maxLobbyListenersCount) {
		console.log('Lobby listener limit reached');
		sendRoomList();
		socket.emit('roomActivity', "The activity feed is disabled, because, the server is very busy at the moment. Sorry about that.");
		socket.disconnect();
	}

	socket.on('subscribeActivities', function(data) {
		activityStream.addListener(socket);
	});

	socket.on('getRoomList', function(data) {
		sendRoomList();
	});

	socket.on('disconnect', function() {
		activityStream.removeListener(socket);
	});
});

var paintIO = require('socket.io').listen(paintPort);

paintIO.enable('browser client minification'); // send minified client
paintIO.enable('browser client etag'); // apply etag caching logic based on version number
paintIO.enable('browser client gzip'); // gzip the file
paintIO.set('log level', 1); // reduce logging

paintIO.sockets.on('connection', function(socket) {
	socket.emit('welcome', '');
	socket.on('connectInfo', function(data) {

		var roomName = data.roomName;
		if (roomName.length>25)
		{
			roomName = roomName.slice(0,25);
		}
		var userName = data.userName;
		if (userName.length>25)
		{
			userName = userName.slice(0,25);
		}

		if (!rooms[roomName]) {
			// create room
			
			var activeRooms = 0;
			for (var r in rooms)
			{
				if (rooms[r].numberOfActiveUsers() > 0)
				{
					activeRooms++;
				}
			}
			
			if (activeRooms >= maxRooms)
			{
				console.log('Room limit reached');
				socket.emit('error', 'There are no free slots available right now. Sorry about that.');
				socket.disconnect();
				return;
			}
			
			rooms[roomName] = new Room(roomName);

			activityStream.postActivity('"' + userName + '" opened room "' + roomName + '".');
		} else {
			
			if (rooms[roomName].numberOfActiveUsers() >= maxUsersPerRoom)
			{
				console.log('Room is full: '+ rooms[roomName].name);
				socket.emit('error', 'The room is full. Sorry about that.');
				socket.disconnect();
				return;
			}				
			
			activityStream.postActivity('"' + userName + '" joined room "' + roomName + '".');
		}

		var room = rooms[roomName];

		var user = room.userForName(userName);
		if (user.socket) {
			socket.emit('error', 'This nickname is already taken.');
			socket.disconnect();
			return;
		}
		user.socket = socket;

		socket.emit('userId', user.id);
		room.emitFrom(user, 'userJoined', user.id);
		console.log("User connected from " + socket.handshake.address.address + ". UserId: " + user.id);

		function sendLayers(currentUserId, currentLayerIndex, roomState) {
			if (currentUserId >= roomState.length) {
				// we've sent all layers for all users
				return;
			}
			if (!roomState[currentUserId] || currentLayerIndex >= roomState[currentUserId].length) {
				// sent all layers for THIS user
				setTimeout(function() {
					sendLayers(currentUserId + 1, 0, roomState);
				}, 0);
				return;
			}

			// only send out those strokes that were there when the user connected
			// the others will have generated their own emits
			if (room.layerStrokes[currentUserId][currentLayerIndex]) {
				socket.emit('layer', room.layerStrokes[currentUserId][currentLayerIndex].slice(0, roomState[currentUserId][currentLayerIndex]));
			}

			// send next layer
			setTimeout(function() {
				sendLayers(currentUserId, currentLayerIndex + 1, roomState);
			}, 0);
		};

		sendLayers(0, 0, room.state());

		socket.on('brushStroke', function(data) {
			if (!room.layerStrokes[data.userId]) {
				room.layerStrokes[data.userId] = [];
			}
			if (!room.layerStrokes[data.userId][data.layerIndex]) {
				room.layerStrokes[data.userId][data.layerIndex] = [];
			}
			data['rev'] = room.layerStrokes[data.userId][data.layerIndex].length;
			room.layerStrokes[data.userId][data.layerIndex].push(data);
			room.emitFrom(user, 'brushStroke', data);
		});
		socket.on('chat', function(data) {
			room.emitFrom(user, 'chat', data);
		});
		socket.on('disconnect', function() {
			room.emitFrom(user, 'userLeft', user.id);
			user.socket = undefined; // release socket
			activityStream.postActivity('"' + userName + '" left room "' + roomName + '".');
		});
	});
});
